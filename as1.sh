#!/bin/bash

#### Initialize arguments
db="$1"
user="$2"
pass="$3"
#########################
echo
tput setaf 100; echo "#####-----> Connecting to database $db <-----#####"
echo
#### Connect to database and run .sql script to create the table
mysql -u "$user" --password="$pass" "$db" < create_author_table.sql
################################################################
echo
tput setaf 10; echo "#####-----> Created table \"author\" <-----#####"

#### Run .csv reader / .sql generator
echo
tput setaf 100; echo "#####-----> Compile Java class <-----#####"
javac DbSqlGen.java
echo
tput setaf 100; echo "#####-----> Run Java bytecode <-----#####"
java DbSqlGen
echo
tput setaf 10; echo "#####-----> Data insertion script generated <-----#####" 
#####################################

#### Insert data into database
mysql -u "$user" --password="$pass" "$db" -e "source insert_authors.sql"
echo
tput setaf 10; echo "#####-----> Inserted data from csv to author table <----####"
###############################

#### Query table and save result to file
mysql -u "$user" --password="$pass" "$db" -e "SELECT * FROM author;" > "$user".txt
mysql -u "$user" --password="$pass" "$db" -e "DROP TABLE IF EXISTS author;"
echo
tput setaf 10; echo "Author table deleted"
####

