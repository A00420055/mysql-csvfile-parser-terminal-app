import java.io.*;
import java.util.*;

/**
 *
 * @author Ildar Abdrashitov
 */
public class DbSqlGen {
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
        //Instantate DbSqlGen to invoke non-static method in the static context
        // Get file`s abs. path
        DbSqlGen dbg = new DbSqlGen();
        String filePath = new File("authors.csv").getAbsolutePath();
        
        //Running file read/write mthod
        try {
        dbg.readCsvGenSql(filePath);}
        catch(IOException ex) {
            System.out.println("IO Error: " + ex);
        }
        
    }
    
    private void readCsvGenSql(String fileName) throws FileNotFoundException, IOException {
        
        //Deletes target .sql file if it exists in the directory
        FileReader file = null;
        File sqlFile = new File("insert_authors.sql");
        if(sqlFile.exists()) {
            sqlFile.delete();
        }
        
        //Init file reader stream
        try{
            file = new FileReader(fileName);
        }
        catch(FileNotFoundException ex) {
            System.err.println("Error reading file: File not found");
        }

        //Init variable and streams
        String line;
        String lastLine;
        String currentLine;
        int lineCount;
        ArrayList<String> linesString;
        BufferedReader breader = null;
        BufferedWriter output = null;
        
        try{
            //Instantiating stream objects and setting default sql statement which will be appended with new lines further
            lineCount = 0;
            linesString = new ArrayList<String>();
            breader = new BufferedReader(file);
            output = new BufferedWriter(new FileWriter(sqlFile));
            output.write("INSERT INTO author (fname, lname, email) VALUES" + "\n");
            
            //Removing the first line (column names), appending each line to the list dynamically, tracking th last line in the read stream
            while((line=breader.readLine()) != null) {
                if(lineCount == 0) {
                    lineCount++;
                }
                else {
                    linesString.add(line);
                }
            }
            
            for (int i = 0; i < linesString.toArray().length; i++) {
                if(i != linesString.toArray().length -1) {
                    currentLine = (String) linesString.toArray()[i];
                    lastLine = null;
                    lineWriter(currentLine, lastLine, output);
                    
                }
                else {
                    currentLine = null;
                    lastLine = (String) linesString.toArray()[i];
                    lineWriter(currentLine, lastLine, output);
                }

                
            }
        }
        catch(IOException ex) {
            System.out.println("IO Error: " + ex);
        }
        //Closing streams
        finally {
            breader.close();
            file.close();
            output.close();
        }
    }
    
    // Line writer method, tokenizes each separate line and generates separate sql values insertion statements
    private void lineWriter(String currentLine, String lastLine, BufferedWriter output) throws IOException {

        try{
            if (lastLine == null) {
                String[] tokens = currentLine.split(",", 3);
                String fname = tokens[0];
                String lname = tokens[1];
                String email = tokens[2];
                String stdValuesLine = "(" + "'" + fname + "'"+" ," + "'" + lname + "'" +" ," + "'" + email + "')," + "\n";
                output.write(stdValuesLine);
            }
            else if(currentLine == null) {
                String[] tokens = lastLine.split(",", 3);
                String fname = tokens[0];
                String lname = tokens[1];
                String email = tokens[2];
                String lastValuesLine = "(" + "'" + fname + "'"+" ," + "'" + lname + "'" +" ," + "'" + email + "');"; 
                output.write(lastValuesLine);
            }
                
        }
        catch(IOException ex) {
            System.err.println("Error writing to file: " + ex);
        }
        
    }
}
